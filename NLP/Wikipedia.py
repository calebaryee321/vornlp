"""
Setting up complex extention attributes. Using URL'S
Given a word doc or file, return a url of all persons, organizations, location, etc.
"""

from spacy.lang.en import English
from spacy.tokens import Doc,Span
import json
nlp = English()

#Defining the getter function
def get_has_number(doc):
    return any(token.like_num for token in doc)

Doc.set_extension('has_number', getter = get_has_number)
doc = nlp("The museum closed for five years in 2012.")
#print('has_number:',doc._.has_number)

def to_html(span,tag):
    # Wrap the span text in a HTML tag and return it
    return "<{tag}>{text}</{tag}>".format(tag=tag, text=span.text)

Span.set_extension('to_html',method = to_html)

doc = nlp('Hello world, this is a sentence.')
span = doc[0:2]
#print(span._.to_html('strong'))



import spacy
from spacy.tokens import Span

nlp = spacy.load('en_core_web_sm')
def get_wikipedia_url(span):
    # Get a Wikipedia URL if the span has one of the labels
    if span.label_ in ( "ORG","LOCATION", "GPE"):
        entity_text = span.text.replace(" ", "_")
        return "https://en.wikipedia.org/w/index.php?search=" + entity_text

Span.set_extension("wikipedia_url", getter=get_wikipedia_url)

#doc = nlp("The University of Maryland  and NOBLIS, LIDL US is a thriving one in the Reston Virginia area, close to various universities such as Noblis,  University of Virginia, Marymount University, American University, Cornell University, Woodrow Wilson High School etc")
file = open(r"C:\Users\m31444\PycharmProjects\MasterP\Spacy\NLP AND GEOTAGGING STUFF\ct.txt", encoding="utf8")
file = file.read()
doc = nlp(file)
#appened each entity recognized in orgs
orgs = []
for ent in doc.ents:# print(ent.text, ent.label_,  ent._.wikipedia_url)
         if(ent.label_ == "ORG" or ent.label_ == 'GPE'):
             orgs.append(ent.text)
             #print(orgs)



"""   
from sklearn.feature_extraction.text import TfidfVectorizer
vectorizer = TfidfVectorizer()
matrix = vectorizer.fit_transform(doc).todense()
matrix2 = vectorizer.fit_transform(doc2).todense()

#print(cosine_similarity(doc,doc2))
"""

#Geolocating
#print(ent.text)
#AIzaSyA2tyeroaVLFuMJpyC_SuqeRpHwJ9bPi2E
import requests
def get_lat_lng(apiKey, address):
    import requests

    url = ('https://maps.googleapis.com/maps/api/geocode/json?address={0}&key={1}'.format(address, apiKey))
    try:
        response = requests.get(url)
        resp_json_payload = response.json()
        #print(json.dumps(resp_json_payload, indent=4))
        #print(resp_json_payload['results'])
        lat = resp_json_payload['results'][0]['geometry']['location']['lat']
        lng = resp_json_payload['results'][0]['geometry']['location']['lng']


    except Exception as e:
        print(e)
        print('ERROR: {}'.format(address))
        lat = 0
        lng = 0


    return [lat, lng]




#def getAddress(name):
apiKey = "AIzaSyA2tyeroaVLFuMJpyC_SuqeRpHwJ9bPi2E"


stored_latlng = []
locName = []
for name in enumerate(orgs):
     key  = str(name)
     # print("This is the key", key)
     locName.append(name[1])
     stored_latlng.append(get_lat_lng(apiKey,key))


address = key



#location = get_lat_lng(apiKey,address)

#lat, lng = get_lat_lng(apiKey,address)
#print(('{} Coordinates:\nLatitude:  {}°\nLongitude: {}°'.format(address,lat, lng)))



"""
#Get Latitudes and Longitudes given a location/
from  geopy.geocoders import Nominatim
geolocator = Nominatim()
address = ent
print(address)
loc = geolocator.geocode(address)
print("latitude is :-" ,loc.latitude,"\nlongtitude is:-" ,loc.longitude)

lat = loc.latitude
long = loc.longitude
"""
import numpy as np
import matplotlib.pyplot as plt

import os
os.environ["PROJ_LIB"] = R"C:\Users\m31444\AppData\Local\Continuum\anaconda3\pkgs\proj4-5.2.0-ha925a31_1\Library\share"
from mpl_toolkits.basemap import Basemap


"""
plt.figure(figsize=(8, 8))
m = Basemap(projection='mill', resolution="c", lat_0=lat, lon_0=long)

m.drawcoastlines()
m.drawcounties(linewidth=2)
m.drawstates(color='b')
m.bluemarble(scale=0.5);
#plt.plot(lat, long, 'ok', markersize=5)
#plt.text(lat, long, ' Seattle', fontsize=12);
plt.show()

"""
"""
m = Basemap(projection='mill',
            llcrnrlat=25,
            llcrnrlon=-130,
            urcrnrlat=50,
            urcrnrlon=-60,
            resolution='l')
m.drawcoastlines()
m.drawcountries(linewidth=2)
m.drawstates(color = 'b')
m.bluemarble()


lati,longi = lat,long
#passing the lattitudes and longitudes into x and y variables
xpt,ypt = m(lati,longi)

m.plot(xpt,ypt, "g^", markersize =30)

plt.show()

"""
#using
import folium
from folium import plugins

# folium_map = folium.Map(
#                             zoom_start=20,
#                             tiles="OpenStreetMap", detect_retina =True)

folium_map = folium.Map( zoom_start=20, tiles="CartoDB dark_matter")


for i,location1 in enumerate(stored_latlng):
    marker = folium.CircleMarker(location1,color = 'teal')
    pU = locName[i]
    print(pU)
    popup = folium.map.Popup(html="Organization: "+"<br>" +str(pU) + " " + str(location1)).add_to(marker)
    marker.add_to(folium_map)

# for i in enumerate(locName):
#         pU = i[1]
#         print(pU)
#         popup = folium.map.Popup(html=str(pU)).add_to(marker)



folium_map.save("my_map6345.html")

"""

for index in location1:
    print(index)
    if index > 0:
        color = "#E37222"  # tangerine
    else:
        color = "#0A8A9F"  # teal
        folium_map = folium.Map(location=location1,
                        zoom_start=13,
                        tiles="CartoDB positron")


marker = folium.CircleMarker(location=location1)
marker.add_to(folium_map)
folium_map.save("my_map3.html")
"""



