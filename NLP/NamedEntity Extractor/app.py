from flask import Flask,render_template,url_for,request
from spacy import displacy
import en_core_web_sm
import re
import pandas as pd
import spacy
from NER import get_displacy_url,get_displacy_text
from wtforms import Form, TextField
import validators

nlp = spacy.load('en_core_web_sm')
app = Flask(__name__ )

@app.route('/')
def index():
	return render_template("index.html")

@app.route('/process', methods=["POST"])
def process():
    display_tags = False
    invalid_url = False
    if request.method == 'POST':
        url = request.form['url_input']
        if validators.url(url):
            displacy = get_displacy_url(url)
            display_tags = True
        else:
            displacy=None
            invalid_url = True
        choice = request.form['taskoption']
        rawtext = request.form['rawtext']
        if rawtext != "":
            invalid_url = False
            doc = nlp(rawtext)
            displacy = get_displacy_text(rawtext)
            display_tags = True
            d = []
            for ent in doc.ents:
                d.append((ent.label_, ent.text))
                df = pd.DataFrame(d, columns=('named entity', 'output'))
                ORG_named_entity = df.loc[df['named entity'] == 'ORG']['output']
                PERSON_named_entity = df.loc[df['named entity'] == 'PERSON']['output']
                GPE_named_entity = df.loc[df['named entity'] == 'GPE']['output']
                LOC_named_entity = df.loc[df['named entity'] == 'LOC']['output']
                FAC_named_entity = df.loc[df['named entity'] == 'FAC']['output']
                PRODUCT_named_entity = df.loc[df['named entity'] == 'PRODUCT']['output']

            if choice == 'organization':
                results = ORG_named_entity
                num_of_results = len(results)
            elif choice == 'person':
                results = PERSON_named_entity
                num_of_results = len(results)
            elif choice == 'geolocation':
                results = GPE_named_entity
                num_of_results = len(results)
            elif choice == 'LOC':
                results = LOC_named_entity
                num_of_results = len(results)
            elif choice == 'FAC':
                results = FAC_named_entity
                num_of_results = len(results)
            elif choice == 'Product':
                results = PRODUCT_named_entity
                num_of_results = len(results)
            else:
                results = [0]
                num_of_results = 0
    return render_template("index.html", tags=display_tags, displacy=displacy, invalid_url=invalid_url)


if __name__ == '__main__':
    app.run(debug=True)