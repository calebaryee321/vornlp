import spacy
from spacy import displacy
from collections import Counter
from bs4 import BeautifulSoup
import requests
import re

nlp = spacy.load("en_core_web_sm")
doc = nlp('European authorities fined Google a record $5.1 billion on Wednesday for abusing its power in the mobile phone market and ordered the company to alter its practices')
#print([(X.text, X.label_) for X in doc.ents])
# token-level entity annotation using the BILUO tagging scheme to describe the entity boundaries.
#print([(X, X.ent_iob_, X.ent_type_) for X in doc])

def url_to_string(url):
    res = requests.get(url)
    html = res.text
    soup = BeautifulSoup(html, 'html5lib')
    for script in soup(["script", "style", 'aside']):
        script.extract()
    return " ".join(re.split(r'[\n\t]+', soup.get_text()))

def get_displacy(text):
    article = nlp(url_to_string(text))
    sentence = [x for x in article.sents]
    return displacy.render(nlp(str(sentence)), page=True ,style='ent')