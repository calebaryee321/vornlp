#Building Web Scrapper

import urllib.request
import re
from bs4 import BeautifulSoup

page_link = "https://python-visualization.github.io/folium/modules.html#module-folium.vector_layers"
page_response = urllib.request.urlopen(page_link, timeout = 5)
try:
    page = urllib.request.urlopen(page_response)
except:
    print("An error occured.")

page_content = BeautifulSoup(page_response, "html.parser")

"""
textContent = []
for i in range(0,2000):
    paragraphs = page_content.find_all('p')[i].text
    textContent.append(paragraphs)
"""