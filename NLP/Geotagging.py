#Code for geotagging task. Able to pick out organizations, universities, etc and map them to a location


import spacy
from spacy.tokens import Span

nlp = spacy.load('en_core_web_sm')
""" 
def get_wikipedia_url(span):
    # Get a Wikipedia URL if the span has one of the labels
    if span.label_ in ( "ORG","LOCATION", "GPE"):
        entity_text = span.text.replace(" ", "_")
        return "https://en.wikipedia.org/w/index.php?search=" + entity_text

Span.set_extension("wikipedia_url", getter=get_wikipedia_url)
"""


#Takes a text or crawler and parses it in here
#doc = nlp("The University of Maryland  and NOBLIS, LIDL US is a thriving one in the Reston Virginia area, close to various universities such as Noblis,  University of Virginia, Marymount University, American University, Cornell University, Woodrow Wilson High School etc")

file = open(r"C:\Users\m31444\PycharmProjects\MasterP\Spacy\NLP AND GEOTAGGING STUFF\ct.txt", encoding="utf8")
file = file.read()
doc = nlp(file)
#appened each entity recognized in orgs
orgs = []
for ent in doc.ents:# print(ent.text, ent.label_,  ent._.wikipedia_url)
         if(ent.label_ == "ORG"):
             orgs.append(ent.text)
             #print(orgs)

#Code for geolocating entities

def get_lat_lng(apiKey, address):
    import requests
    import json
    url = ('https://maps.googleapis.com/maps/api/geocode/json?address={0}&key={1}'.format(address, apiKey))
    try:
        response = requests.get(url)
        resp_json_payload = response.json()
        #print(json.dumps(resp_json_payload, indent=4))
        #print(resp_json_payload['results'])
        lat = resp_json_payload['results'][0]['geometry']['location']['lat']
        lng = resp_json_payload['results'][0]['geometry']['location']['lng']

    except Exception as e:
        print(e)
        print('ERROR: {}'.format(address))
        lat = 0
        lng = 0
    return [lat, lng]

#The API key and address
apiKey = "AIzaSyA2tyeroaVLFuMJpyC_SuqeRpHwJ9bPi2E"

stored_latlng = []
locName = []
for name in enumerate(orgs):
     key  = str(name)
     # print("This is the key", key)
     locName.append(name[1])
     stored_latlng.append(get_lat_lng(apiKey,key))

stored_latlng=[x for x  in stored_latlng if x!= [0,0]]

print(stored_latlng)

#Using folium for the geolocating
import folium

folium_map = folium.Map(
    zoom_start=13,
    tiles="CartoDB dark_matter")


for i,location1 in enumerate(stored_latlng):
    marker = folium.CircleMarker(location1, color='teal')
    name = locName[i]
    popup = folium.map.Popup(html="Organization: " + "<br>" + str(name) + " " + str(location1)).add_to(marker)
    marker.add_to(folium_map)

folium_map.save("my_map62.html")




